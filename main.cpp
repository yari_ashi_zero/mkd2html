#include <iostream>
#include <fstream>
#include <string>
#include <regex>

using namespace std;

struct markdown_pattern
{
    //common
    string head_space;

    //single line.
    string header_pattern;
    string list_pattern;
    string line_pattern;

    //multiple lines.
    string bold_pattern;
    string italic_pattern;
    string syntax1_pattern;
};

struct html_format {
    string content;
    bool prev_list_state;
    int list_level;
};

void initstruct(
        markdown_pattern *pattern)
{
    pattern -> head_space = "^[ |　|\t]";

    //single line
    pattern -> header_pattern = "(^[ |\t|]*#{1,6})([\\W|]*\\w*)";
    pattern -> line_pattern = "^((\\*\\s*){3,}|(\\-\\s*){3,})$";

    //multiple lines.
    pattern -> list_pattern = "^(\\s*)\\*?\\s{1}(\\w*)";
    //pattern -> bold_pattern = "\\*\\*(.*\\\\*.*)\\*\\*";
    //pattern -> italic_pattern = "\\*([\\w|\\W|\\d|\\D])\\*";
}

void initstruct(
        html_format *hformat)
{
    hformat -> prev_list_state = 0;
}

void debugstruct(
        markdown_pattern *pattern)
{
    printf("--- BEGIN: struct debug console ---\n");
    printf("header pattern: %s\n", pattern -> header_pattern.c_str());
    printf("bold pattern: %s\n", pattern -> bold_pattern.c_str());
    printf("italic pattern: %s\n", pattern -> italic_pattern.c_str());
    printf("syntax1 pattern: %s\n", pattern -> syntax1_pattern.c_str());
    printf("--- END: struct debug console ---\n");
}

vector<string> split(const string &str, char delim) {
    istringstream iss(str);
    string tmp;
    vector<string> res;
    while (getline(iss, tmp, delim)) {
        res.push_back(tmp);
    }
    return res;
}

int mkdanalysis(
        string line,
        markdown_pattern *pattern,
        html_format *hformat)
{

    regex reg(pattern -> list_pattern);
    smatch sm;
    if (regex_search(line, sm, reg)) {
        char converted[256];
        if (hformat -> prev_list_state) {
            //level diff
            int diff = string(sm[1]).size() == 0 ?
                0 : string(sm[1]).size() / 4;
            int current_level = diff - hformat -> list_level;
            if (current_level > 0) {
                for (int i = 0; i < current_level; i++) {
                    hformat -> content += "<ul>\n";
                }
                hformat -> prev_list_state = 1;
                hformat -> list_level = diff;
                sprintf(converted, "<li>%s</li>\n", string(sm[2]).c_str());
                hformat -> content += string(converted);
            } else if (current_level < 0) {
                for (int i = 0; i < abs(current_level); i++) {
                    hformat -> content += "</ul>\n";
                }
                hformat -> prev_list_state = 1;
                hformat -> list_level = diff;
                sprintf(converted, "<li>%s</li>\n", string(sm[2]).c_str());
                hformat -> content += string(converted);
            }
        } else {
            hformat -> content += "<ul>\n";
            hformat -> prev_list_state = 1;
            hformat -> list_level = 0;
            sprintf(converted, "<li>%s</li>\n", string(sm[2]).c_str());
            hformat -> content += string(converted);
        }
        return 0;
    } else if (hformat -> prev_list_state) {
        for (int i = 0; i < hformat -> list_level; i++) {
            hformat -> content += "</ul>\n";
        }
        hformat -> prev_list_state = 0;
        hformat -> list_level = 0;
        hformat -> content += "</ul>\n";
    }

    reg = regex(pattern -> header_pattern);
    if (regex_search(line, sm, reg)) {
        int hlevel = split(sm[1], '#').size();
        reg = regex(pattern -> head_space);
        string replaced;
        char converted[256];
        replaced = regex_replace(string(sm[2]), reg, "");
        sprintf(converted, "<h%d>%s</h%d>\n", hlevel, replaced.c_str(), hlevel);
        hformat -> content += string(converted);
        return 0;
    }

    reg = regex(pattern -> line_pattern);
    if (regex_search(line, sm, reg)) {
        hformat -> content += "<hr>\n";
        return 0;
    }

    /*reg = regex(pattern -> bold_pattern);
    if (regex_search(line, sm, reg)) {
        char converted[256];
        sprintf(converted, "<b>%s</b>", string(sm[1]).c_str());
        line = regex_replace(line, reg, string(converted));
    }*/

    /*reg = regex(pattern -> italic_pattern);
    if (regex_search(line, sm, reg)) {
        char converted[256];
        sprintf(converted, "<i>%s</i>", string(sm[1]).c_str());
        line = regex_replace(line, reg, string(converted));
    }*/

    hformat -> content += line + "\n";

    return 0;
}

int mkd2bitbucket(
        string fname,
        markdown_pattern *pattern)
{
    ifstream ifs(fname.c_str());
    if (ifs.fail()) { return -1; }

    html_format hformat;
    initstruct(&hformat);

    string line;
    int linecnt = 1;
    while (getline(ifs, line)) {

        printf("--- LINE NO: %d\n", linecnt);
        printf("INPUT TEXT: %s\n", line.c_str());

        //analysis markdown style
        mkdanalysis(line, pattern, &hformat);

        printf("\n");
        linecnt++;
    }

    printf("---------- CONTENT ----------\n");
    printf("%s", hformat.content.c_str());

}

int main(
        int arg,
        char *args[])
{
    markdown_pattern pattern;
    initstruct(&pattern);
    debugstruct(&pattern);

    mkd2bitbucket("test.md", &pattern);
}

